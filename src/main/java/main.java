import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class main {
    public static void main(String[] args) throws IOException, InterruptedException {

        Jedis jd = new Jedis("localhost");

        Scanner Input = new Scanner(System.in);

        String op = "";

        String Valido = null;

        do {
            System.out.println("Digite 1 para Cadastrar apostador");
            System.out.println("Digite 2 para Apostar");
            System.out.println("Digite 3 para Ranking");
            System.out.println("Digite 0 para Sair");
            op = Input.nextLine();

            switch (op) {


                case "1":


                    /* Usuario */

                    System.out.println("Coloque um Apelido (Unico)");
                    String Apelido = Input.nextLine();

                    Boolean  Verifica =  jd.sismember("Apelido",Apelido);
                    if (Apelido != null && Verifica == false  ){
                        jd.sadd("Apelido", Apelido);

                        Thread.sleep(300);
                        System.out.println("Digite seu Nome Completo");
                        String Nome = Input.nextLine();
                        if (Nome != null) {
                            jd.hset("Usuario:" + Apelido, "Nome_Completo", Nome);
                        }

                        System.out.println("Digite seu Data Nascimento (DD/MM/YYYY)");
                        String Data_Nascimento = Input.nextLine();
                        if (Data_Nascimento.length() == 10 && Data_Nascimento.contains("/") && Data_Nascimento != null) {
                            jd.hset("Usuario:" + Apelido, "Data_Nascimento", Data_Nascimento);
                        }
                        else {
                            System.out.println("Data escrita errada");
                        }

                        System.out.println("Digite seu Genero ( M ou F )");
                        String Genero = Input.nextLine();
                        if (Genero != null) {
                            jd.hset("Usuario:" + Apelido, "Genero", Genero);
                        }

                        int IdEndereco = 0;

                        do {
                            IdEndereco++;

                            jd.hset("Usuario:" + Apelido, "IdEndereco",String.valueOf(IdEndereco));
                            System.out.println("Digite seu País");
                            String Pais = Input.nextLine();
                            if (Pais != null) {
                                jd.hset("Usuario:" + Apelido, "IdEndereco"+IdEndereco+"Pais", Pais);
                            }

                            System.out.println("Digite seu Estado (UF)");
                            String Estado = Input.nextLine();
                            if (Estado != null) {
                                jd.hset("Usuario:" + Apelido, "IdEndereco"+IdEndereco+"Estado", Estado);
                            }

                            System.out.println("Digite seu Cidade");
                            String Cidade = Input.nextLine();
                            if (Cidade != null) {
                                jd.hset("Usuario:" + Apelido, "IdEndereco"+IdEndereco+"Cidade", Cidade);
                            }

                            System.out.println("Digite seu Rua");
                            String Rua = Input.nextLine();
                            if (Rua != null) {
                                jd.hset("Usuario:" + Apelido, "IdEndereco"+IdEndereco+"Rua", Rua);
                            }

                            System.out.println("Digite seu Complemento");
                            String Complemento = Input.nextLine();
                            if (Complemento != null) {
                                jd.hset("Usuario:" + Apelido, "IdEndereco"+IdEndereco+"Complemento", Complemento);
                            }

                            System.out.println("Digite seu Codigo Postal (00000-000)");
                            String Codigo_postal = Input.nextLine();
                            if (Codigo_postal.contains("-") && Codigo_postal != null) {
                                jd.hset("Usuario:" + Apelido, "IdEndereco"+IdEndereco+"Codigo_postal", Codigo_postal);
                            } else {
                                System.out.println("Escrito errado");
                            }
                            System.out.println("Deseja colocar mais um endereço S ou N");
                        }while ( Input.nextLine().equalsIgnoreCase("S"));

                        System.out.println("Nome Completo "+jd.hget("Usuario:" + Apelido, "Nome_Completo"));
                        System.out.println("Data_Nascimento "+jd.hget("Usuario:" + Apelido, "Data_Nascimento"));
                        System.out.println("Genero "+jd.hget("Usuario:" + Apelido, "Genero"));

                        int NumEndereco = Integer.parseInt(jd.hget("Usuario:" + Apelido, "IdEndereco"));
                        for (int i = 1 ; i <= NumEndereco;i++) {
                            System.out.println("-------------------------------------------");
                            System.out.println("Endereço "+i);

                            System.out.println("Pais " + jd.hget("Usuario:" + Apelido, "IdEndereco"+i+"Pais"));
                            System.out.println("Estado " + jd.hget("Usuario:" + Apelido, "IdEndereco"+i+"Estado"));
                            System.out.println("Cidade " + jd.hget("Usuario:" + Apelido, "IdEndereco"+i+"Cidade"));
                            System.out.println("Rua " + jd.hget("Usuario:" + Apelido, "IdEndereco"+i+"Rua"));
                            System.out.println("Complemento " + jd.hget("Usuario:" + Apelido, "IdEndereco"+i+"Complemento"));
                            System.out.println("Codigo postal " + jd.hget("Usuario:" + Apelido, "IdEndereco"+i+"Codigo_postal"));
                        }
                    }
                    else
                    {
                        System.out.println("\nApelido já existe\n");
                    }
                    /* Fim Usuario */

                    break;


                case "2":


                    /* Rodada */
                    Random Aletario = new Random();
                    for (int i = 1; i <= 38; i++) {
                        for (int j = 1; j <= 10; j++) {
                            jd.hset("Rodada:" + i, "IdPartida:" + j, String.valueOf(Aletario.nextInt(3)));
                            jd.hset("Rodada:" + i, "IdPartida:" + j+"Time1", String.valueOf(Aletario.nextInt(5)));
                            jd.hset("Rodada:" + i, "IdPartida:" + j+"Time2", String.valueOf(Aletario.nextInt(5)));
                        }
                    }

                    /* Fim Rodada */

                    /*Cadastrar time*/
                    jd.hset("Time","0","Atletico");
                    jd.hset("Time","1","Coritiba");
                    jd.hset("Time","2","Paraná");
                    jd.hset("Time","3","Palmeiras");
                    jd.hset("Time","4","São Paulo");
                    jd.hset("Time","5","Vasco");
                    /*Cadastrar time*/

                    /* Apostar */
                    System.out.println("Digite seu apelido");
                    Apelido = Input.nextLine();

                    Valido = "N";

                    for (String apelido :jd.smembers("Apelido")) {
                        if (apelido.equalsIgnoreCase(Apelido)){
                            Valido = "S";
                        }
                    }

                    if(Valido.equalsIgnoreCase( "S")) {
                        String OpAposta = "";

                        do {
                            System.out.println("---------------------------Apostar-------------------------------------");

                            System.out.println("Digite Rodada");
                            String Rodada = Input.nextLine();

                            System.out.println("Digite Partida");
                            String Partida = Input.nextLine();

                            System.out.println("Resultado '0' para empate , '1' para apostar Time 1  e '2' para apostar Time 2");
                            String Aposta = Input.nextLine();

                            jd.hset("Aposta:" + Apelido, "Rodada:" + Rodada + "Partida:" + Partida, Aposta);

                            String ApostaResult = jd.hget("Rodada:" + Rodada, "IdPartida:" + Partida);

                            System.out.println("Aposta: " + Aposta + " Resultado: " + ApostaResult);

                            if (ApostaResult.equalsIgnoreCase(Aposta)) {
                                System.out.println("Acertou");
                                if(Aposta.equalsIgnoreCase("1")) {
                                    String Idtime = jd.hget("Rodada:" + Rodada, "IdPartida:" + Partida + "Time1");
                                    System.out.println("Time que você apostou:"+jd.hget("Time",Idtime));
                                }
                                if(Aposta.equalsIgnoreCase("2")) {
                                    String Idtime = jd.hget("Rodada:" + Rodada, "IdPartida:" + Partida + "Time2");
                                    System.out.println("Time que você apostou:"+jd.hget("Time",Idtime));
                                }
                                if(Aposta.equalsIgnoreCase("0")) {
                                    System.out.println("Empate");

                                    String Idtime1 = jd.hget("Rodada:" + Rodada, "IdPartida:" + Partida + "Time1");
                                    System.out.println("Time A:"+jd.hget("Time",Idtime1));
                                    String Idtime2 = jd.hget("Rodada:" + Rodada, "IdPartida:" + Partida + "Time2");
                                    System.out.println("Time B:"+jd.hget("Time",Idtime2));
                                }
                            }
                            else
                            {
                                System.out.println("Errou");
                            }

                            System.out.println("Seja fazer mais uma aposta S ou N ");

                            OpAposta = Input.nextLine();

                        } while (OpAposta.equalsIgnoreCase("s"));
                    }
                    else
                    {
                        System.out.println("Apelido não existe");
                    }
                    break;
                /*Fim Apostar */


                case "3":


                    /* Ranking */
                    for (String apelido :jd.smembers("Apelido")) {

                        int Pontos = 0;
                        for (int i = 1; i <= 38; i++) {
                            for (int j = 1; j <= 10; j++) {

                                String ApostaResult = jd.hget("Aposta:" + apelido, "Rodada:" + i + "Partida:" + j);
                                String ResultadoJogo = jd.hget("Rodada:" + i, "IdPartida:" + j);

                                try
                                {
                                    if (ApostaResult.equalsIgnoreCase(ResultadoJogo)) {
                                        Pontos = Pontos + 1;
                                    }

                                }
                                catch (Exception e) {
                                }
                            }
                        }
                        // System.out.println("Apelido "+apelido + " Pontos"+Pontos);
                        jd.zadd("Ranking", Pontos, apelido);
                    }

                    int cont = 1;

                    Set<Tuple> Rank = jd.zrevrangeWithScores("Ranking", 0, -1);
                    for (Tuple rank : Rank) {
                        System.out.println("Ranking: " + cont +
                                "° | Nome: " + rank.getElement()+
                                " | Pontos: "+ (int) rank.getScore());
                        cont++;
                    }

                    break;
                case "0":
                    op = "0";
                    break;
                default:
                    System.out.println("Valor não existe");
                    break;
            }
        }while (op != "0");
        /* Fim Ranking */

        Input.close();

    }
}
