import redis.clients.jedis.Jedis;

import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class test {
    public static void main(String[] args) {

        Jedis jd = new Jedis("localhost");

        Scanner Input = new Scanner(System.in);

        /* Usuario */
        System.out.println("Coloque um Apelido");
        String Apelido = Input.nextLine();

        jd.sadd("Apelido", Apelido);

        System.out.println(jd.smembers("Apelido").toString());

        System.out.println("Digite seu Nome Completo");
        String Nome = Input.nextLine();
//        String Nome = "Theo";
        jd.hset("Usuario:"+Apelido ,"Nome_Completo" , Nome);

        System.out.println("Digite seu Data Nascimento");
        String Data_Nascimento = Input.nextLine();
//        String Data_Nascimento = "07/01/1994";
        if(Data_Nascimento.length() == 9 && Data_Nascimento.contains("/")) {
            jd.hset("Usuario:" + Apelido, "Data_Nascimento", Data_Nascimento);
        }

        System.out.println("Digite seu Genero ( M ou F )");
        String Genero = Input.nextLine();
//        String Genero = "M";
        jd.hset("Usuario:"+Apelido ,"Genero", Genero);

        System.out.println("Digite seu País");
        String Pais = Input.nextLine();
//        String Pais = "BR";
        jd.hset("Usuario:"+Apelido ,"Pais", Pais);

        System.out.println("Digite seu Estado");
        String Estado = Input.nextLine();
//        String Estado = "PR";
        jd.hset("Usuario:"+Apelido ,"Estado", Estado);

        System.out.println("Digite seu Cidade");
        String Cidade = Input.nextLine();
//        String Cidade = "Curitiba";
        jd.hset("Usuario:"+Apelido ,"Cidade", Cidade);

        System.out.println("Digite seu Rua");
        String Rua = Input.nextLine();
//        String Rua = "Rua Osorio";
        jd.hset("Usuario:"+Apelido ,"Rua", Rua);

        System.out.println("Digite seu Completo");
        String Complemento = Input.nextLine();
        //String Complemento = "Casa";
        jd.hset("Usuario:"+Apelido ,"Complemento", Complemento);

        System.out.println("Digite seu Codigo Postal");
        String Codigo_postal = Input.nextLine();
//        String Codigo_postal = "31231-031";
        if(Codigo_postal.length() == 9 && Codigo_postal.contains("-")) {
            jd.hset("Usuario:" + Apelido, "Codigo_postal", Codigo_postal);
        }

        /* Fim Usuario */

        /* Rodada */

        Random Aletario = new Random();

        for(int i = 1 ; i <= 38 ; i++){
            for(int j = 1 ; j <= 10 ; j++) {
                jd.hset("Rodada:"+i ,"IdPartida:"+j , String.valueOf(Aletario.nextInt(3)));
            }
        }

        /* Fim Rodada */

        /* Apostar */

        for (int i = 1; i <=3 ; i++){
            for (int j = 1; j <=3 ; j++) {

                /*  System.out.println("Digite Rodada");
                String Rodada = Input.nextLine();

                System.out.println("Digite Partida");
                String Partida = Input.nextLine();*/

                System.out.println("Resultado '0' para empate , '1' para apostar Time 1  e '2' para apostar Time 2");
                String Resultado = Input.nextLine();

                jd.hset("Aposta:" + Apelido, "Rodada:"+i+" Partida:"+j , Resultado);

            }
        }

        /* Fim Apostar */

        /* Ranking */

        int Ranking = 0;

        for(int i = 1 ; i <= 3 ; i++ ){
            for(int j = 1 ; j <= 3 ; j++ ){

                String Aposta = jd.hget("Aposta:"+Apelido, "Rodada:" + i + " Partida:" + j);
                String Resultado = jd.hget("Rodada:"+i ,"IdPartida:"+j);

                //System.out.println("Aposta:"+Aposta+" Resultado:"+Resultado);

                if ( Aposta.trim().equalsIgnoreCase( Resultado.trim() ) ){
                    System.out.println("Acertou");
                    Ranking = Ranking + 1 ;
                }
            }
        }

        System.out.println("Pontos "+ Apelido + " : " + Ranking);

        jd.zadd("Ranking", Ranking , Apelido);

        System.out.println("Ranking Resultado: " + jd.zrevrange("Ranking",0,-1));

        Set<String> Rank = jd.zrevrange("Ranking",0,-1);

        String[] Score = new String[Rank.size()];

        System.out.println(Score[0]);

        /* Fim Ranking */

        Input.close();

    }
}
